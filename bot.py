#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os #for importing env variables
from twitchio.ext import commands
from dotenv import load_dotenv

load_dotenv()

class Bot(commands.Bot):

    def __init__(self):
        # Initialise our Bot with our access token, prefix and a list of channels to join on boot...
        # prefix can be a callable, which returns a list of strings or a string...
        # initial_channels can also be a callable which returns a list of strings...
        super().__init__(token=os.environ['TOKEN'],
                         prefix='?',
                         initial_channels=[os.environ['CHANNEL']])

    async def event_ready(self):
        # Notify us when everything is ready!
        # We are logged in and ready to chat and use commands...
        print(f'Logged in as | {self.nick}')
        print(f'User id is | {self.user_id}')
        #ws = bot._ws #this is only needed to send messages within the event_ready
        #await ws.send_privmsg(os.environ['CHANNEL'], f"/me has landed")

    @commands.command()
    async def hello(self, ctx: commands.Context):
        # Here we have a command hello, we can invoke our command with our prefix and command name
        # e.g ?hello
        # We can also give our commands aliases (different names) to invoke with.

        # Send a hello back!
        # Sending a reply back to the channel is easy... Below is an example.
        await ctx.send(f'Hola {ctx.author.name}!')

if __name__ == "__main__":
    bot = Bot()
    bot.run()
